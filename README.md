# Direct & Factored Deletion #

Prototype implementation of direct and factored deletion algorithms to learn Bayesian network parameters from incomplete data under the MCAR and MAR assumptions. These algorithms are consistent, yet they only require a single pass over the data, and no inference in the Bayesian network.

## Publication ##

Guy Van den Broeck, Karthika Mohan, Arthur Choi, Adnan Darwiche, Judea Pearl. 
**[Efficient Algorithms for Bayesian Network Parameter Learning from Incomplete Data](https://lirias.kuleuven.be/bitstream/123456789/500017/1/deletion-uai15.pdf)**, 
In *Proceedings of the 31st Conference on Uncertainty in Artificial Intelligence (UAI)*, 2015.

## Contact ##

### Website ###
[http://reasoning.cs.ucla.edu/deletion/](http://reasoning.cs.ucla.edu/deletion/)

### Main contact###

Guy Van den Broeck  
Department of Computer Science  
UCLA  
[http://www.guyvandenbroeck.com](http://www.guyvandenbroeck.com)  
[guyvdb@cs.ucla.edu](mailto:guyvdb@cs.ucla.edu)

## Contributors ##

* [Guy Van den Broeck](http://www.guyvandenbroeck.com)
* [Arthur Choi](http://web.cs.ucla.edu/~aychoi/)

## License ##

This source code is licensed under the Apache License, Version 2.0: 
http://www.apache.org/licenses/LICENSE-2.0

This software uses the inference library inflib.jar, which is
provided by the Automated Reasoning Group at UCLA.  inflib.jar is
licensed only for non-commercial, research and educational use.